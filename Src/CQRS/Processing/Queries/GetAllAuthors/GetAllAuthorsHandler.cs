﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Common.Interfaces.Queries;
using DAL.DbContexts;
using Processing.Queries.GetAllAuthors.DTO;

namespace Processing.Queries.GetAllAuthors
{
    /// <summary>
    /// Handler of getting all authors.
    /// </summary>
    public class GetAllAuthorsHandler : IQueryHandler<GetAllAuthorsQuery, IEnumerable<Author>>
    {
        /// <summary>
        /// Data context.
        /// </summary>
        protected CQRSDataContext DataContext { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAllAuthorsHandler"/>.
        /// </summary>
        /// <param name="dataContext">Data context.</param>
        public GetAllAuthorsHandler(CQRSDataContext dataContext)
        {
            DataContext = dataContext;
        }
        /// <inheritdoc/>
        public async Task<IEnumerable<Author>> Handle(GetAllAuthorsQuery query)
        {
            IEnumerable<Author> returnValue;
            returnValue = await DataContext.Authors
                .Select(a => new Author(a.Id, a.Firstname, a.Secondname, a.Birthday, a.Gender))
                .ToListAsync();
            return returnValue;
        }
    }
}