﻿
namespace Common.Enums
{
    /// <summary>
    /// Genres.
    /// </summary>
    public enum Genres
    {
        /// <summary>
        /// Not set.
        /// </summary>
        None = 0,
        /// <summary>
        /// Crime.
        /// </summary>
        Crime = 1,
        /// <summary>
        /// Detective fiction.
        /// </summary>
        DetectiveFiction = 2,
        /// <summary>
        /// Science fiction.
        /// </summary>
        ScienceFiction = 3,
        /// <summary>
        /// Post-apocalyptic.
        /// </summary>
        PostApocalyptic = 4,
        /// <summary>
        /// Distopia.
        /// </summary>
        Distopia = 5,
        /// <summary>
        /// Cyberpunk.
        /// </summary>
        Cyberpunk = 6,
        /// <summary>
        /// Fantasy.
        /// </summary>
        Fantasy = 7,
        /// <summary>
        /// Romantic novel.
        /// </summary>
        RomanticNovel = 8,
        /// <summary>
        /// Western.
        /// </summary>
        Western = 9,
        /// <summary>
        /// Horror.
        /// </summary>
        Horror = 10,
        /// <summary>
        /// Classic.
        /// </summary>
        Classic = 11,
        /// <summary>
        /// Fairy tale.
        /// </summary>
        FairyTale = 12,
        /// <summary>
        /// Folklore.
        /// </summary>
        Folklore = 13,
        /// <summary>
        /// Historical fiction.
        /// </summary>
        HistoricalFiction = 14,
        /// <summary>
        /// Humor.
        /// </summary>
        Humor = 15,
        /// <summary>
        /// Mystery.
        /// </summary>
        Mystery = 16,
        /// <summary>
        /// Picture book.
        /// </summary>
        PictureBook = 17,
        /// <summary>
        /// Thriller.
        /// </summary>
        Thriller = 18,
        /// <summary>
        /// Biograpy.
        /// </summary>
        Biograpy = 19,
        /// <summary>
        /// Autobiography.
        /// </summary>
        Autobiography = 20,
        /// <summary>
        /// Essay.
        /// </summary>
        Essay = 21,
        /// <summary>
        /// User's guide.
        /// </summary>
        UsersGuide = 22,
        /// <summary>
        /// Journalism.
        /// </summary>
        Journalism = 23,
        /// <summary>
        /// Memoir.
        /// </summary>
        Memoir = 24,
        /// <summary>
        /// Reference book.
        /// </summary>
        ReferenceBook = 25,
        /// <summary>
        /// How-to book.
        /// </summary>
        HowToBook = 26,
        /// <summary>
        /// Textbook.
        /// </summary>
        Textbook = 27,
        /// <summary>
        /// Academic paper.
        /// </summary>
        AcademicPaper = 28,
        /// <summary>
        /// Encyclopedia.
        /// </summary>
        Encyclopedia = 29,
        /// <summary>
        /// Dictionary.
        /// </summary>
        Dictionary = 30,
        /// <summary>
        /// Popular science.
        /// </summary>
        PopularScience = 31,
        /// <summary>
        /// Photograph.
        /// </summary>
        Photograph = 32,
        /// <summary>
        /// Technical writing.
        /// </summary>
        TechnicalWriting = 33,
        /// <summary>
        /// Cookbook.
        /// </summary>
        Cookbook = 34,
    }
}