﻿using System.Threading.Tasks;

namespace Common.Interfaces.Queries
{
    /// <summary>
    /// Contract of query handler.
    /// </summary>
    /// <typeparam name="T">Type of handling query.</typeparam>
    /// <typeparam name="TResult">Type of query result.</typeparam>
    public interface IQueryHandler<in T, TResult>
        where T : IQuery
    {
        /// <summary>
        /// Handle query.
        /// </summary>
        /// <param name="query">Query.</param>
        /// <returns>Result of query.</returns>
        Task<TResult> Handle(T query);
    }
}