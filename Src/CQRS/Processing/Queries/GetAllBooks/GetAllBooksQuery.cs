﻿using Common.Interfaces.Queries;

namespace Processing.Queries.GetAllBooks
{
    /// <summary>
    /// Query for get all books.
    /// </summary>
    public class GetAllBooksQuery : IQuery
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAllBooksQuery"/>.
        /// </summary>
        public GetAllBooksQuery()
        {
        }
    }
}