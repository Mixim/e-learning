﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BooksApi.Models
{
    /// <summary>
    /// Book.
    /// </summary>
    public class Book
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        /// <summary>
        /// Name.
        /// </summary>
        [BsonElement("Name")]
        public string BookName { get; set; }
        /// <summary>
        /// Price.
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// Category.
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// Author.
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/>.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="bookName">Name.</param>
        /// <param name="price">Price.</param>
        /// <param name="category">Category.</param>
        /// <param name="author">Author.</param>
        public Book(string id, string bookName, decimal price, string category, string author)
        {
            Id = id;
            BookName = bookName;
            Price = price;
            Category = category;
            Author = author;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/>.
        /// </summary>
        /// <param name="bookName">Name.</param>
        /// <param name="price">Price.</param>
        /// <param name="category">Category.</param>
        /// <param name="author">Author.</param>
        public Book(string bookName, decimal price, string category, string author)
            : this(default, bookName,
                  price, category,
                  author)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/>.
        /// </summary>
        public Book()
            : this(default, default,
                  default, default,
                  default)
        {
        }
    }
}