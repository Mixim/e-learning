﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Queries
{
    /// <summary>
    /// Contract of query dispatcher.
    /// </summary>
    public interface IQueryDispatcher
    {
        /// <summary>
        /// Dispatch query.
        /// </summary>
        /// <typeparam name="T">Type of query.</typeparam>
        /// <typeparam name="TResult">Type of returning result.</typeparam>
        /// <param name="query">Query.</param>
        /// <returns>Task of query dispatching.</returns>
        Task<TResult> Dispatch<T, TResult>(T query)
            where T : IQuery;
    }
}