﻿
namespace Common.Enums
{
    /// <summary>
    /// Genders.
    /// </summary>
    public enum Genders
    {
        /// <summary>
        /// Female.
        /// </summary>
        Female = 1,
        /// <summary>
        /// Male.
        /// </summary>
        Male = 2,
    }
}
