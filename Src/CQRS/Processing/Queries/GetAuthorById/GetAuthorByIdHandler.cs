﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Common.Interfaces.Queries;
using DAL.DbContexts;
using Processing.Queries.GetAuthorById.DTO;
using AuthorDal = DAL.Entities.Author;
using AuthorBookDal = DAL.Entities.AuthorBook;

namespace Processing.Queries.GetAuthorById
{
    /// <summary>
    /// Handler of getting author by identifier.
    /// </summary>
    public class GetAuthorByIdHandler : IQueryHandler<GetAuthorByIdQuery, Author>
    {
        /// <summary>
        /// Data context.
        /// </summary>
        protected CQRSDataContext DataContext { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAuthorByIdHandler"/>.
        /// </summary>
        /// <param name="dataContext">Data context.</param>
        public GetAuthorByIdHandler(CQRSDataContext dataContext)
        {
            DataContext = dataContext;
        }
        /// <inheritdoc/>
        public async Task<Author> Handle(GetAuthorByIdQuery query)
        {
            IEnumerable<AuthorDal> filteredAuthors;
            Author returnValue;

            filteredAuthors = DataContext.Authors
                .Include(a => a.Books)
                .ThenInclude(a => a.Book)
                .Where(a => a.Id == query.Id)
                .AsNoTracking()
                .AsEnumerable();
            returnValue = filteredAuthors
                .Select(a => ConvertDbEntityToDTO(a))
                .SingleOrDefault();
            return returnValue;
        }

        protected Author ConvertDbEntityToDTO(AuthorDal authorDal)
        {
            IList<Book> books;
            Author returnValue;

            books = new List<Book>();
            foreach (AuthorBookDal currentAuthorBook in authorDal.Books.DefaultIfEmpty())
            {
                books.Add(new Book(currentAuthorBook.Book.Id, currentAuthorBook.Book.Title, currentAuthorBook.Book.PublicationDate, currentAuthorBook.Book.Genre));
            }
            returnValue = new Author(authorDal.Id, authorDal.Firstname,
                authorDal.Secondname, authorDal.Birthday,
                authorDal.Gender, books);
            return returnValue;
        }
    }
}