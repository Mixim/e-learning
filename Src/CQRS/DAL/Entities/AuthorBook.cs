﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    /// <summary>
    /// Link between <see cref="Author"/> and <see cref="Book"/>.
    /// </summary>
    [Table("Authors_Of_Books")]
    public class AuthorBook
    {
        /// <summary>
        /// <see cref="Author"/> identifier.
        /// </summary>
        public ulong AuthorId { get; set; }
        /// <summary>
        /// Identifier of <see cref="Book"/>.
        /// </summary>
        public ulong BookId { get; set; }
        /// <summary>
        /// Author.
        /// </summary>
        public Author Author { get; set; }
        /// <summary>
        /// Book.
        /// </summary>
        public Book Book { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorBook"/>.
        /// </summary>
        /// <param name="authorId"><see cref="Author"/> identifier.</param>
        /// <param name="bookId">Identifier of <see cref="Book"/>.</param>
        /// <param name="author">Author.</param>
        /// <param name="book">Book.</param>
        public AuthorBook(ulong authorId, ulong bookId, Author author, Book book)
        {
            AuthorId = authorId;
            BookId = bookId;
            Author = author;
            Book = book;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorBook"/>.
        /// </summary>
        public AuthorBook()
            : this(default, default,
                  default, default)
        {
        }
    }
}
