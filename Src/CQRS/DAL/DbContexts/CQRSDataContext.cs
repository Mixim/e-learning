﻿using Microsoft.EntityFrameworkCore;
using DAL.Entities;

namespace DAL.DbContexts
{
    /// <summary>
    /// Context of database for CQRS.
    /// </summary>
    public class CQRSDataContext : DbContext
    {
        /// <summary>
        /// Authors.
        /// </summary>
        public DbSet<Author> Authors { get; set; }
        /// <summary>
        /// Books.
        /// </summary>
        public DbSet<Book> Books { get; set; }
        /// <summary>
        /// Authors of books.
        /// </summary>
        public DbSet<AuthorBook> AuthorsOfBooks { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CQRSDataContext"/>.
        /// </summary>
        /// <param name="options">Options.</param>
        public CQRSDataContext(DbContextOptions<CQRSDataContext> options)
            : base(options)
        {
        }
        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthorBook>()
                .HasKey(ab => new { ab.AuthorId, ab.BookId });
            modelBuilder.Entity<AuthorBook>()
                .HasOne<Author>(a => a.Author)
                .WithMany(b => b.Books)
                .HasForeignKey(b => b.AuthorId);
            modelBuilder.Entity<AuthorBook>()
                .HasOne<Book>(b => b.Book)
                .WithMany(a => a.Authors)
                .HasForeignKey(b => b.BookId);
        }
    }
}