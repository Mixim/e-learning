﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Enums;

namespace DAL.Entities
{
    /// <summary>
    /// Book.
    /// </summary>
    [Table("Books")]
    public class Book
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Title.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Publication date.
        /// </summary>
        public DateTime PublicationDate { get; set; }
        /// <summary>
        /// Genre.
        /// </summary>
        public Genres Genre { get; set; }
        /// <summary>
        /// Authors.
        /// </summary>
        public IList<AuthorBook> Authors { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/>.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="title">Title.</param>
        /// <param name="publicationDate">Publication date.</param>
        /// <param name="genre">Genre.</param>
        /// <param name="authors">Authors.</param>
        public Book(ulong id, string title,
            DateTime publicationDate, Genres genre,
            IList<AuthorBook> authors)
        {
            Id = id;
            Title = title;
            PublicationDate = publicationDate;
            Genre = genre;
            Authors = authors;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/>.
        /// </summary>
        /// <param name="title">Title.</param>
        /// <param name="publicationDate">Publication date.</param>
        /// <param name="genre">Genre.</param>
        /// <param name="authors">Authors.</param>
        public Book(string title,
            DateTime publicationDate, Genres genre,
            IList<AuthorBook> authors)
            : this(default, title,
                  publicationDate, genre,
                  authors)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/>.
        /// </summary>
        public Book()
            : this(default, default,
                  default, default,
                  new List<AuthorBook>())
        {
        }
    }
}
