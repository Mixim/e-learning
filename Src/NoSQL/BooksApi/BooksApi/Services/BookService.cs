﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using BooksApi.Models;
using BooksApi.Models.Contracts;

namespace BooksApi.Services
{
    /// <summary>
    /// Service for working with <see cref="Book"/>.
    /// </summary>
    public class BookService
    {
        /// <summary>
        /// Collection of books.
        /// </summary>
        protected IMongoCollection<Book> Books { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="BookService"/>.
        /// </summary>
        public BookService(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            IMongoDatabase database = client.GetDatabase(settings.DatabaseName);
            Books = database.GetCollection<Book>(settings.BooksCollectionName);
        }
        /// <summary>
        /// Get all books.
        /// </summary>
        /// <returns>All books.</returns>
        public IEnumerable<Book> Get()
        {
            return Books.Find(b => true)
                .ToEnumerable();
        }
        /// <summary>
        /// Get book.
        /// </summary>
        /// <param name="id">Identifier of the book.</param>
        /// <returns>Book.</returns>
        public async Task<Book> Get(string id)
        {
            return await Books.Find(b => b.Id == id)
                .SingleOrDefaultAsync();
        }
        /// <summary>
        /// Create new book.
        /// </summary>
        /// <param name="book">New book.</param>
        /// <returns>Result of creation.</returns>
        public async Task<Book> Create(Book book)
        {
            await Books.InsertOneAsync(book);
            return book;
        }
        /// <summary>
        /// Update book.
        /// </summary>
        /// <param name="id">Identifier of the book.</param>
        /// <param name="bookIn">New info for book with <paramref name="id"/>.</param>
        public async void Update(string id, Book bookIn)
        {
            bookIn.Id = id;
            await Books.ReplaceOneAsync(b => b.Id == id, bookIn);
        }
        /// <summary>
        /// Delete book.
        /// </summary>
        /// <param name="id">Identifier of the book.</param>
        public async void Remove(string id)
        {
            await Books.DeleteOneAsync(b => b.Id == id);
        }
    }
}