﻿using System.ComponentModel.DataAnnotations;
using Common.Interfaces.Queries;

namespace Processing.Queries.GetBookById
{
    /// <summary>
    /// Query for get book by identifier.
    /// </summary>
    public class GetBookByIdQuery : IQuery
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        [Required]
        public ulong? Id { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetBookByIdQuery"/>.
        /// </summary>
        /// <param name="id">Identifier.</param>
        public GetBookByIdQuery(ulong? id)
        {
            Id = id;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetBookByIdQuery"/>.
        /// </summary>
        public GetBookByIdQuery()
            : this(default)
        {
        }
    }
}