﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Common.Interfaces.Queries;
using Processing.Queries.Exceptions;

namespace Processing.Queries
{
    /// <summary>
    /// Query dispatcher.
    /// </summary>
    public class QueryDispatcher : IQueryDispatcher
    {
        /// <summary>
        /// Service provider.
        /// </summary>
        protected IServiceProvider ServiceProvider { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryDispatcher"/>.
        /// </summary>
        /// <param name="serviceProvider">Service provider.</param>
        public QueryDispatcher(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }
        /// <inheritdoc/>
        public async Task<TResult> Dispatch<T, TResult>(T query)
            where T : IQuery
        {
            IQueryHandler<T, TResult> currentHandler;
            TResult returnValue;

            currentHandler = ServiceProvider.GetService<IQueryHandler<T, TResult>>();
            if (currentHandler == null)
            {
                throw new QueryHandlerNotFoundException(typeof(T), typeof(TResult));
            }
            else
            {
                returnValue = await currentHandler.Handle(query);
            }
            return returnValue;
        }
    }
}