﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Common.Enums;

namespace DAL.Entities
{
    /// <summary>
    /// Author.
    /// </summary>
    [Table("Authors")]
    public class Author
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Firstname.
        /// </summary>
        public string Firstname { get; set; }
        /// <summary>
        /// Secondname.
        /// </summary>
        public string Secondname { get; set; }
        /// <summary>
        /// Birthday.
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// Gender.
        /// </summary>
        public Genders Gender { get; set; }
        /// <summary>
        /// Author's books.
        /// </summary>
        public IList<AuthorBook> Books { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Author"/>.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="secondname">Secondname.</param>
        /// <param name="birthday">Birthday.</param>
        /// <param name="gender">Gender.</param>
        /// <param name="books">Author's books.</param>
        public Author(ulong id, string firstname,
            string secondname, DateTime birthday,
            Genders gender, IList<AuthorBook> books)
        {
            Id = id;
            Firstname = firstname;
            Secondname = secondname;
            Birthday = birthday;
            Gender = gender;
            Books = books;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Author"/>.
        /// </summary>
        /// <param name="firstname">Firstname.</param>
        /// <param name="secondname">Secondname.</param>
        /// <param name="birthday">Birthday.</param>
        /// <param name="gender">Gender.</param>
        /// <param name="books">Author's books.</param>
        public Author(string firstname,
            string secondname, DateTime birthday,
            Genders gender, IList<AuthorBook> books)
            : this(default, firstname,
                  secondname, birthday,
                  gender, books)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Author"/>.
        /// </summary>
        public Author()
            : this(default, default,
                  default, default,
                  default, new List<AuthorBook>())
        {
        }
    }
}