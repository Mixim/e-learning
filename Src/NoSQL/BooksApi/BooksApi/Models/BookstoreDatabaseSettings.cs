﻿using BooksApi.Models.Contracts;

namespace BooksApi.Models
{
    /// <summary>
    /// Settings of bookstore database.
    /// </summary>
    public class BookstoreDatabaseSettings : IBookstoreDatabaseSettings
    {
        /// <inheritdoc/>
        public string BooksCollectionName { get ; set; }
        /// <inheritdoc/>
        public string ConnectionString { get; set; }
        /// <inheritdoc/>
        public string DatabaseName { get; set; }
    }
}