﻿using System.ComponentModel.DataAnnotations;
using Common.Interfaces.Queries;

namespace Processing.Queries.GetAuthorById
{
    /// <summary>
    /// Query for get author by identifier.
    /// </summary>
    public class GetAuthorByIdQuery : IQuery
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        [Required]
        public ulong? Id { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAuthorByIdQuery"/>.
        /// </summary>
        /// <param name="id">Identifier.</param>
        public GetAuthorByIdQuery(ulong? id)
        {
            Id = id;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAuthorByIdQuery"/>.
        /// </summary>
        public GetAuthorByIdQuery()
            : this(default)
        {
        }
    }
}