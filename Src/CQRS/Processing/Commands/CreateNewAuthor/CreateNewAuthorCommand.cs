﻿using System;
using System.Text.Json.Serialization;
using Common.Enums;
using Common.Interfaces.Commands;

namespace Processing.Commands.CreateNewAuthor
{
    /// <summary>
    /// Command of author creation.
    /// </summary>
    public class CreateNewAuthorCommand : ICommand
    {
        /// <summary>
        /// Firstname.
        /// </summary>
        [JsonPropertyName("firstname")]
        public string Firstname { get; set; }
        /// <summary>
        /// Secondname.
        /// </summary>
        [JsonPropertyName("secondname")]
        public string Secondname { get; set; }
        /// <summary>
        /// Birthday.
        /// </summary>
        [JsonPropertyName("birthday")]
        public DateTime Birthday { get; set; }
        /// <summary>
        /// Gender.
        /// </summary>
        [JsonPropertyName("gender")]
        public Genders Gender { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewAuthorCommand"/>.
        /// </summary>
        /// <param name="firstname">Firstname.</param>
        /// <param name="secondname">Secondname.</param>
        /// <param name="birthday">Birthday.</param>
        /// <param name="gender">Gender.</param>
        public CreateNewAuthorCommand(string firstname, string secondname, DateTime birthday, Genders gender)
        {
            Firstname = firstname;
            Secondname = secondname;
            Birthday = birthday;
            Gender = gender;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewAuthorCommand"/>.
        /// </summary>
        public CreateNewAuthorCommand()
            : this(default, default, default, default)
        {
        }
    }
}
