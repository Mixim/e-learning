﻿using Common.Interfaces.Queries;

namespace Processing.Queries.GetAllAuthors
{
    /// <summary>
    /// Query for get all authors.
    /// </summary>
    public class GetAllAuthorsQuery : IQuery
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAllAuthorsQuery"/>.
        /// </summary>
        public GetAllAuthorsQuery()
        {
        }
    }
}