﻿using System;

namespace Processing.Commands.Exceptions
{
    /// <summary>
    /// Represents error of resolving command handler.
    /// </summary>
    public class CommandHandlerNotFoundException : Exception
    {
        /// <summary>
        /// Format of <see cref="ToString"/> output.
        /// </summary>
        protected const string ToStringFormat = "Command handler for type \"{0}\" could not be found.";

        /// <summary>
        /// Type of unresolved command handler.
        /// </summary>
        protected Type TypeOfCommandHandler { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandHandlerNotFoundException"/>.
        /// </summary>
        /// <param name="typeOfCommandHandler">Type of unresolved command handler.</param>
        public CommandHandlerNotFoundException(Type typeOfCommandHandler)
        {
            TypeOfCommandHandler = typeOfCommandHandler;
        }
        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format(ToStringFormat, TypeOfCommandHandler);
        }
    }
}
