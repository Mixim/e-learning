﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Common.Interfaces.Commands;
using Processing.Commands.Exceptions;

namespace Processing.Commands
{
    /// <summary>
    /// Command dispatcher.
    /// </summary>
    public class CommandDispatcher : ICommandDispatcher
    {
        /// <summary>
        /// Service provider.
        /// </summary>
        protected IServiceProvider ServiceProvider { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandDispatcher"/>.
        /// </summary>
        /// <param name="serviceProvider">Service provider.</param>
        public CommandDispatcher(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }
        /// <inheritdoc/>
        public async Task Dispatch<T>(T command)
            where T : ICommand
        {
            ICommandHandler<T> currentHandler;

            currentHandler = ServiceProvider.GetService<ICommandHandler<T>>();
            if (currentHandler == null)
            {
                throw new CommandHandlerNotFoundException(typeof(T));
            }
            else
            {
                await currentHandler.Execute(command);
            }
        }
    }
}
