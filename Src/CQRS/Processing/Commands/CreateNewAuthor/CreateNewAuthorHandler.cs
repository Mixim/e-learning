﻿using System.Threading.Tasks;
using Common.Interfaces.Commands;
using DAL.DbContexts;
using DAL.Entities;

namespace Processing.Commands.CreateNewAuthor
{
    /// <summary>
    /// Handler of new author creation.
    /// </summary>
    public class CreateNewAuthorHandler : ICommandHandler<CreateNewAuthorCommand>
    {
        /// <summary>
        /// Data context.
        /// </summary>
        protected CQRSDataContext DataContext { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewAuthorHandler"/>.
        /// </summary>
        /// <param name="dataContext">Data context.</param>
        public CreateNewAuthorHandler(CQRSDataContext dataContext)
        {
            DataContext = dataContext;
        }
        /// <inheritdoc/>
        public async Task Execute(CreateNewAuthorCommand command)
        {
            DataContext.Authors.Add(new Author(command.Firstname, command.Secondname, command.Birthday, command.Gender, null));
            await DataContext.SaveChangesAsync();
        }
    }
}