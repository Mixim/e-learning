﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Common.Interfaces.Queries;
using DAL.DbContexts;
using Processing.Queries.GetBookById.DTO;
using AuthorBookDal = DAL.Entities.AuthorBook;
using BookDal = DAL.Entities.Book;

namespace Processing.Queries.GetBookById
{
    /// <summary>
    /// Handler of getting book by identifier.
    /// </summary>
    public class GetBookByIdHandler : IQueryHandler<GetBookByIdQuery, Book>
    {
        /// <summary>
        /// Data context.
        /// </summary>
        protected CQRSDataContext DataContext { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetBookByIdHandler"/>.
        /// </summary>
        /// <param name="dataContext">Data context.</param>
        public GetBookByIdHandler(CQRSDataContext dataContext)
        {
            DataContext = dataContext;
        }
        /// <inheritdoc/>
        public async Task<Book> Handle(GetBookByIdQuery query)
        {
            IEnumerable<BookDal> filteredBooks;
            Book returnValue;

            filteredBooks = DataContext.Books
                .Include(b => b.Authors)
                .ThenInclude(b => b.Author)
                .Where(b => b.Id == query.Id)
                .AsNoTracking()
                .AsEnumerable();
            returnValue = filteredBooks
                .Select(b => ConvertDbEntityToDTO(b))
                .SingleOrDefault();
            return returnValue;
        }
        /// <summary>
        /// Convert <see cref="BookDal"/> to <see cref="Book"/>.
        /// </summary>
        /// <param name="bookDal">Representation of book on DAL.</param>
        /// <returns>Representation of book as DTO.</returns>
        protected Book ConvertDbEntityToDTO(BookDal bookDal)
        {
            IList<Author> authors;
            Book returnValue;

            authors = new List<Author>();
            foreach (AuthorBookDal currentAuthorBook in bookDal.Authors.DefaultIfEmpty())
            {
                authors.Add(new Author(currentAuthorBook.Author.Id,
                    currentAuthorBook.Author.Firstname,
                    currentAuthorBook.Author.Secondname,
                    currentAuthorBook.Author.Birthday,
                    currentAuthorBook.Author.Gender));
            }
            returnValue = new Book(bookDal.Id, bookDal.Title,
                bookDal.PublicationDate, bookDal.Genre,
                authors);
            return returnValue;
        }
    }
}