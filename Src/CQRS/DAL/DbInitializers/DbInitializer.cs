﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Enums;
using DAL.DbContexts;
using DAL.Entities;

namespace DAL.DbInitializers
{
    /// <summary>
    /// Initializer of <see cref="CQRSDataContext"/> database.
    /// </summary>
    public static class DbInitializer
    {
        /// <summary>
        /// Initialize database of <paramref name="dbContext"/> by default values.
        /// </summary>
        /// <param name="dbContext">Database context.</param>
        public static void Initialize(CQRSDataContext dbContext)
        {
            dbContext.Database.EnsureCreated();

            if (!dbContext.Authors.Any())
            {
                var authors = new List<Author>();
                authors.Add(new Author(1, "John", "Tolkien", new DateTime(1892, 1, 3), Genders.Male, new List<AuthorBook>()));
                authors.Add(new Author(2, "Aleksandr", "Solzhenitsyn", new DateTime(1918, 12, 11), Genders.Male, new List<AuthorBook>()));
                authors.Add(new Author(3, "Simone", "Beauvoir", new DateTime(1908, 01, 9), Genders.Female, new List<AuthorBook>()));
                authors.Add(new Author(4, "Anne", "Frank", new DateTime(1929, 06, 12), Genders.Female, new List<AuthorBook>()));
                dbContext.Authors.AddRange(authors);

                dbContext.SaveChanges();

                var books = new List<Book>();
                books.Add(new Book(1, "The Lord of the Rings: The Fellowship of the Ring", new DateTime(1954, 7, 29), Genres.Fantasy, new List<AuthorBook>()));
                books.Add(new Book(2, "The Lord of the Rings: The Two Towers", new DateTime(1954, 11, 11), Genres.Fantasy, new List<AuthorBook>()));
                books.Add(new Book(3, "The Lord of the Rings: The Return of the King", new DateTime(1955, 10, 20), Genres.Fantasy, new List<AuthorBook>()));
                books.Add(new Book(4, "The Gulag Archipelago", new DateTime(1973, 1, 1), Genres.HistoricalFiction, new List<AuthorBook>()));
                books.Add(new Book(5, "The Second Sex", new DateTime(1949, 1, 1), Genres.HistoricalFiction, new List<AuthorBook>()));
                books.Add(new Book(6, "The Diary of Anne Frank", new DateTime(1947, 6, 25), Genres.Autobiography, new List<AuthorBook>()));
                dbContext.Books.AddRange(books);

                dbContext.SaveChanges();

                var authorsBooks = new List<AuthorBook>();
                authorsBooks.Add(new AuthorBook(1, 1, authors.Single(a => a.Id == 1), books.Single(b => b.Id == 1)));
                authorsBooks.Add(new AuthorBook(1, 2, authors.Single(a => a.Id == 1), books.Single(b => b.Id == 2)));
                authorsBooks.Add(new AuthorBook(1, 3, authors.Single(a => a.Id == 1), books.Single(b => b.Id == 3)));
                authorsBooks.Add(new AuthorBook(2, 4, authors.Single(a => a.Id == 2), books.Single(b => b.Id == 4)));
                authorsBooks.Add(new AuthorBook(3, 5, authors.Single(a => a.Id == 3), books.Single(b => b.Id == 5)));
                authorsBooks.Add(new AuthorBook(4, 6, authors.Single(a => a.Id == 4), books.Single(b => b.Id == 6)));
                dbContext.AuthorsOfBooks.AddRange(authorsBooks);

                dbContext.SaveChanges();
            }
        }
    }
}
