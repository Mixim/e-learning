using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Common.Interfaces.Commands;
using Common.Interfaces.Queries;
using DAL.DbContexts;
using Processing.Commands;
using Processing.Commands.CreateNewAuthor;
using Processing.Commands.CreateNewAuthorBook;
using Processing.Commands.CreateNewBook;
using Processing.Queries;
using Processing.Queries.GetAllAuthors;
using Processing.Queries.GetAllAuthors.DTO;
using Processing.Queries.GetAllBooks;
using Processing.Queries.GetAllBooks.DTO;
using Processing.Queries.GetAuthorById;
using Processing.Queries.GetBookById;
using GetAuthorByIdResult = Processing.Queries.GetAuthorById.DTO.Author;
using GetBookByIdResult = Processing.Queries.GetBookById.DTO.Book;

namespace CQRS
{
    /// <summary>
    /// Startup of microservice.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Name of connection string for <see cref="CQRSDataContext"/>.
        /// </summary>
        protected const string NameOfConnectionStringForCQRSDataContext = "CQRSConnection";

        /// <summary>
        /// Configuration of microservice.
        /// </summary>
        protected IConfiguration Configuration { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/>.
        /// </summary>
        /// <param name="configuration">Configuration of microservice.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        /// <summary>
        /// Configure services.
        /// </summary>
        /// <param name="services">Services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CQRSDataContext>(options =>
                options.UseMySql(Configuration.GetConnectionString(NameOfConnectionStringForCQRSDataContext)));

            services.AddMvc();
            ConfigureCommandServices(services);
            ConfigureQueryServices(services);
        }
        /// <summary>
        /// Configure microservice.
        /// </summary>
        /// <param name="app">Application builder.</param>
        /// <param name="env">Environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        /// <summary>
        /// Configure command services.
        /// </summary>
        /// <param name="services">Services.</param>
        public void ConfigureCommandServices(IServiceCollection services)
        {
            services.AddScoped<ICommandHandler<CreateNewAuthorCommand>, CreateNewAuthorHandler>();
            services.AddScoped<ICommandHandler<CreateNewBookCommand>, CreateNewBookHandler>();
            services.AddScoped<ICommandHandler<CreateNewAuthorBookCommand>, CreateNewAuthorBookHandler>();
            services.AddScoped<ICommandDispatcher, CommandDispatcher>();
        }
        /// <summary>
        /// Configure query services.
        /// </summary>
        /// <param name="services">Services.</param>
        public void ConfigureQueryServices(IServiceCollection services)
        {
            services.AddScoped<IQueryHandler<GetAllAuthorsQuery, IEnumerable<Author>>, GetAllAuthorsHandler>();
            services.AddScoped<IQueryHandler<GetAllBooksQuery, IEnumerable<Book>>, GetAllBooksHandler>();
            services.AddScoped<IQueryHandler<GetAuthorByIdQuery, GetAuthorByIdResult>, GetAuthorByIdHandler>();
            services.AddScoped<IQueryHandler<GetBookByIdQuery, GetBookByIdResult>, GetBookByIdHandler>();
            services.AddScoped<IQueryDispatcher, QueryDispatcher>();
        }
    }
}