﻿using System.Threading.Tasks;

namespace Common.Interfaces.Commands
{
    /// <summary>
    /// Contract of command dispatcher.
    /// </summary>
    public interface ICommandDispatcher
    {
        /// <summary>
        /// Dispatch command.
        /// </summary>
        /// <typeparam name="T">Type of command.</typeparam>
        /// <param name="command">Command.</param>
        /// <returns>Task of command dispatching.</returns>
        Task Dispatch<T>(T command)
            where T : ICommand;
    }
}
