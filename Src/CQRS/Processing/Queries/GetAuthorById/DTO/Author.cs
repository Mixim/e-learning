﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Enums;

namespace Processing.Queries.GetAuthorById.DTO
{
    /// <summary>
    /// Author.
    /// </summary>
    public class Author
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Firstname.
        /// </summary>
        public string Firstname { get; set; }
        /// <summary>
        /// Secondname.
        /// </summary>
        public string Secondname { get; set; }
        /// <summary>
        /// Birthday.
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// Gender.
        /// </summary>
        public Genders Gender { get; set; }
        /// <summary>
        /// Author's books.
        /// </summary>
        public IEnumerable<Book> Books { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="Author"/>.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="firstname">Firstname.</param>
        /// <param name="secondname">Secondname.</param>
        /// <param name="birthday">Birthday.</param>
        /// <param name="gender">Gender.</param>
        /// <param name="books">Author's books.</param>
        public Author(ulong id, string firstname,
            string secondname, DateTime birthday,
            Genders gender, IEnumerable<Book> books)
        {
            Id = id;
            Firstname = firstname;
            Secondname = secondname;
            Birthday = birthday;
            Gender = gender;
            Books = books;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Author"/>.
        /// </summary>
        public Author()
            : this(default, default,
                  default, default,
                  default, Enumerable.Empty<Book>())
        {
        }
    }
}