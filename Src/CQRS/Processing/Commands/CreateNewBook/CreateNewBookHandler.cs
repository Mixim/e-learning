﻿using System.Threading.Tasks;
using Common.Interfaces.Commands;
using DAL.DbContexts;
using DAL.Entities;

namespace Processing.Commands.CreateNewBook
{
    /// <summary>
    /// Handler of new book creation.
    /// </summary>
    public class CreateNewBookHandler : ICommandHandler<CreateNewBookCommand>
    {
        /// <summary>
        /// Data context.
        /// </summary>
        protected CQRSDataContext DataContext { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewBookHandler"/>.
        /// </summary>
        /// <param name="dataContext">Data context.</param>
        public CreateNewBookHandler(CQRSDataContext dataContext)
        {
            DataContext = dataContext;
        }
        /// <inheritdoc/>
        public async Task Execute(CreateNewBookCommand command)
        {
            DataContext.Books.Add(new Book(command.Title, command.PublicationDate, command.Genre, null));
            await DataContext.SaveChangesAsync();
        }
    }
}