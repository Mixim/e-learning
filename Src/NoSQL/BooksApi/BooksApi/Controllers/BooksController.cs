﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using BooksApi.Services;
using BooksApi.Models;
using System.Threading.Tasks;

namespace BooksApi.Controllers
{
    /// <summary>
    /// Controller for processing requests to books.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        /// <summary>
        /// Service for working with <see cref="Book"/>.
        /// </summary>
        protected BookService BookService { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="BooksController"/>.
        /// </summary>
        /// <param name="bookService">Service for working with <see cref="Book"/>.</param>
        public BooksController(BookService bookService)
        {
            BookService = bookService;
        }
        /// <summary>
        /// Get all books.
        /// </summary>
        /// <returns>Books.</returns>
        public ActionResult<IEnumerable<Book>> Get()
        {
            return Ok(BookService.Get());
        }
        /// <summary>
        /// Get book.
        /// </summary>
        /// <param name="id">Identifier of the book.</param>
        /// <returns>Book.</returns>
        [HttpGet("{id:length(24)}", Name = "GetBook")]
        public async Task<ActionResult<Book>> Get(string id)
        {
            Book book;
            book = await BookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }
            else
            {
                return book;
            }
        }
        /// <summary>
        /// Create new book.
        /// </summary>
        /// <param name="book">New book.</param>
        /// <returns>Result of creation.</returns>
        [HttpPost]
        public async Task<ActionResult<Book>> Create(Book book)
        {
            await BookService.Create(book);

            return CreatedAtRoute("GetBook", new { id = book.Id.ToString() }, book);
        }
        /// <summary>
        /// Update book.
        /// </summary>
        /// <param name="id">Identifier of the book.</param>
        /// <param name="bookIn">New info for book with <paramref name="id"/>.</param>
        /// <returns>Result of updating.</returns>
        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update(string id, Book bookIn)
        {
            Book book;
            book = await BookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }
            else
            {
                BookService.Update(id, bookIn);

                return NoContent();
            }
        }
        /// <summary>
        /// Delete book.
        /// </summary>
        /// <param name="id">Identifier of the book.</param>
        /// <returns>Result of deleting.</returns>
        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete(string id)
        {
            Book book;
            book = await BookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }
            else
            {
                BookService.Remove(book.Id);
                return NoContent();
            }
        }
    }
}