﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Common.Interfaces.Commands;
using Common.Interfaces.Queries;
using Processing.Commands.CreateNewAuthor;
using Processing.Commands.CreateNewAuthorBook;
using Processing.Commands.CreateNewBook;
using Processing.Queries.GetAllAuthors;
using Processing.Queries.GetAllAuthors.DTO;
using Processing.Queries.GetAllBooks;
using Processing.Queries.GetAllBooks.DTO;
using Processing.Queries.GetAuthorById;
using Processing.Queries.GetBookById;
using AuthorById = Processing.Queries.GetAuthorById.DTO.Author;
using BookById = Processing.Queries.GetBookById.DTO.Book;

namespace CQRS.Controllers
{
    /// <summary>
    /// Home controller.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        /// <summary>
        /// Command dispatcher.
        /// </summary>
        protected ICommandDispatcher CommandDispatcher { get; }
        /// <summary>
        /// Query dispatcher.
        /// </summary>
        protected IQueryDispatcher QueryDispatcher { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/>.
        /// </summary>
        /// <param name="commandDispatcher">Command dispatcher.</param>
        /// <param name="queryDispatcher">Query dispatcher.</param>
        public HomeController(ICommandDispatcher commandDispatcher,
            IQueryDispatcher queryDispatcher)
        {
            CommandDispatcher = commandDispatcher;
            QueryDispatcher = queryDispatcher;
        }
        /// <summary>
        /// Create new author.
        /// </summary>
        /// <param name="createNewAuthorCommand">Command for create new author.</param>
        [HttpPost("createNewAuthor")]
        public async Task CreateNewAuthor([FromBody] CreateNewAuthorCommand createNewAuthorCommand)
        {
            await CommandDispatcher.Dispatch(createNewAuthorCommand);
        }
        /// <summary>
        /// Create new book.
        /// </summary>
        /// <param name="createNewBookCommand">Command for create new book.</param>
        [HttpPost("createNewBook")]
        public async Task CreateNewBook([FromBody] CreateNewBookCommand createNewBookCommand)
        {
            await CommandDispatcher.Dispatch(createNewBookCommand);
        }
        /// <summary>
        /// Create link between author and book.
        /// </summary>
        /// <param name="createNewAuthorBookCommand">Command for create link between author and book.</param>
        [HttpPost("createNewAuthorBook")]
        public async Task CreateNewAuthorBook([FromBody] CreateNewAuthorBookCommand createNewAuthorBookCommand)
        {
            await CommandDispatcher.Dispatch(createNewAuthorBookCommand);
        }
        /// <summary>
        /// Get all authors.
        /// </summary>
        /// <param name="getAllAuthorsQuery">Query for get all authors.</param>
        /// <returns>Task for get all authors.</returns>
        [HttpGet("getAllAuthors")]
        [ProducesResponseType(typeof(IEnumerable<Author>), (int)HttpStatusCode.OK)]
        public async Task<IEnumerable<Author>> GetAllAuthors([FromQuery] GetAllAuthorsQuery getAllAuthorsQuery)
        {
            IEnumerable<Author> returnValue;
            returnValue = await QueryDispatcher.Dispatch<GetAllAuthorsQuery, IEnumerable<Author>>(getAllAuthorsQuery);
            return returnValue;
        }
        /// <summary>
        /// Get all books.
        /// </summary>
        /// <param name="getAllBooksQuery">Query for get all books.</param>
        /// <returns>Task for get all books.</returns>
        [HttpGet("getAllBooks")]
        [ProducesResponseType(typeof(IEnumerable<Book>), (int)HttpStatusCode.OK)]
        public async Task<IEnumerable<Book>> GetAllBooks([FromQuery] GetAllBooksQuery getAllBooksQuery)
        {
            IEnumerable<Book> returnValue;
            returnValue = await QueryDispatcher.Dispatch<GetAllBooksQuery, IEnumerable<Book>>(getAllBooksQuery);
            return returnValue;
        }
        /// <summary>
        /// Get author by identifier.
        /// </summary>
        /// <param name="getAuthorByIdQuery">Query for get author by identifier.</param>
        /// <returns>Task for get author by identifier.</returns>
        [HttpGet("getAuthorById")]
        [ProducesResponseType(typeof(AuthorById), (int)HttpStatusCode.OK)]
        public async Task<AuthorById> GetAuthorById([FromQuery] GetAuthorByIdQuery getAuthorByIdQuery)
        {
            AuthorById returnValue;
            returnValue = await QueryDispatcher.Dispatch<GetAuthorByIdQuery, AuthorById>(getAuthorByIdQuery);
            return returnValue;
        }
        /// <summary>
        /// Get book by identifier.
        /// </summary>
        /// <param name="getBookByIdQuery">Query for get book by identifier.</param>
        /// <returns>Task for get book by identifier.</returns>
        [HttpGet("getBookById")]
        [ProducesResponseType(typeof(BookById), (int)HttpStatusCode.OK)]
        public async Task<BookById> GetBookById([FromQuery] GetBookByIdQuery getBookByIdQuery)
        {
            BookById returnValue;
            returnValue = await QueryDispatcher.Dispatch<GetBookByIdQuery, BookById>(getBookByIdQuery);
            return returnValue;
        }
    }
}