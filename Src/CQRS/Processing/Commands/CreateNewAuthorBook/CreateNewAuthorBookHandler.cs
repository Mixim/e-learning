﻿using System.Threading.Tasks;
using Common.Interfaces.Commands;
using DAL.DbContexts;
using DAL.Entities;

namespace Processing.Commands.CreateNewAuthorBook
{
    /// <summary>
    /// Handler for creation link between author and book.
    /// </summary>
    public class CreateNewAuthorBookHandler : ICommandHandler<CreateNewAuthorBookCommand>
    {
        /// <summary>
        /// Data context.
        /// </summary>
        protected CQRSDataContext DataContext { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewAuthorBookHandler"/>.
        /// </summary>
        /// <param name="dataContext">Data context.</param>
        public CreateNewAuthorBookHandler(CQRSDataContext dataContext)
        {
            DataContext = dataContext;
        }
        /// <inheritdoc/>
        public async Task Execute(CreateNewAuthorBookCommand command)
        {
            DataContext.AuthorsOfBooks.Add(new AuthorBook(command.AuthorId, command.BookId, null, null));
            await DataContext.SaveChangesAsync();
        }
    }
}