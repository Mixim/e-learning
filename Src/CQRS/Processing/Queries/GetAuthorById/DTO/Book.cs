﻿using System;
using Common.Enums;

namespace Processing.Queries.GetAuthorById.DTO
{
    /// <summary>
    /// Book.
    /// </summary>
    public class Book
    {
        /// <summary>
        /// Identifier.
        /// </summary>
        public ulong Id { get; set; }
        /// <summary>
        /// Title.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Publication date.
        /// </summary>
        public DateTime PublicationDate { get; set; }
        /// <summary>
        /// Genre.
        /// </summary>
        public Genres Genre { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/>.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <param name="title">Title.</param>
        /// <param name="publicationDate">Publication date.</param>
        /// <param name="genre">Genre.</param>
        public Book(ulong id, string title, DateTime publicationDate, Genres genre)
        {
            Id = id;
            Title = title;
            PublicationDate = publicationDate;
            Genre = genre;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/>.
        /// </summary>
        public Book()
            : this(default, default,
                  default, default)
        {
        }
    }
}