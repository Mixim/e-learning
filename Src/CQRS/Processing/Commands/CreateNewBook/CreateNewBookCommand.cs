﻿using System;
using System.Text.Json.Serialization;
using Common.Enums;
using Common.Interfaces.Commands;

namespace Processing.Commands.CreateNewBook
{
    /// <summary>
    /// Command of book creation.
    /// </summary>
    public class CreateNewBookCommand : ICommand
    {
        /// <summary>
        /// Title.
        /// </summary>
        [JsonPropertyName("title")]
        public string Title { get; set; }
        /// <summary>
        /// Publication date.
        /// </summary>
        [JsonPropertyName("publicationDate")]
        public DateTime PublicationDate { get; set; }
        /// <summary>
        /// Genre.
        /// </summary>
        [JsonPropertyName("genre")]
        public Genres Genre { get; set; }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewBookCommand"/>.
        /// </summary>
        /// <param name="title">Title.</param>
        /// <param name="publicationDate">Publication date.</param>
        /// <param name="genre">Genre.</param>
        public CreateNewBookCommand(string title, DateTime publicationDate,
            Genres genre)
        {
            Title = title;
            PublicationDate = publicationDate;
            Genre = genre;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewBookCommand"/>.
        /// </summary>
        public CreateNewBookCommand()
            : this(default, default,
                  default)
        {
        }
    }
}
