﻿using System.Threading.Tasks;

namespace Common.Interfaces.Commands
{
    /// <summary>
    /// Contract of command handler.
    /// </summary>
    /// <typeparam name="T">Type of handling command.</typeparam>
    public interface ICommandHandler<in T> where T : ICommand
    {
        /// <summary>
        /// Execute command.
        /// </summary>
        /// <param name="command">Command.</param>
        /// <returns>Task of command handling.</returns>
        Task Execute(T command);
    }
}