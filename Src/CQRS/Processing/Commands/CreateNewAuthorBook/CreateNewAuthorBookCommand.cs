﻿using Common.Interfaces.Commands;

namespace Processing.Commands.CreateNewAuthorBook
{
    /// <summary>
    /// Command for creation link between author and book.
    /// </summary>
    public class CreateNewAuthorBookCommand : ICommand
    {
        /// <summary>
        /// Author's identifier.
        /// </summary>
        public ulong AuthorId { get; set; }
        /// <summary>
        /// Identifier of book.
        /// </summary>
        public ulong BookId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewAuthorBookCommand"/>.
        /// </summary>
        /// <param name="authorId">Author's identifier.</param>
        /// <param name="bookId">Identifier of book.</param>
        public CreateNewAuthorBookCommand(ulong authorId, ulong bookId)
        {
            AuthorId = authorId;
            BookId = bookId;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateNewAuthorBookCommand"/>.
        /// </summary>
        public CreateNewAuthorBookCommand()
            : this(default, default)
        {
        }
    }
}