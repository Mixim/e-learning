﻿using System;

namespace Processing.Queries.Exceptions
{
    /// <summary>
    /// Represents error of resolving query handler.
    /// </summary>
    public class QueryHandlerNotFoundException : Exception
    {
        /// <summary>
        /// Format of <see cref="ToString"/> output.
        /// </summary>
        protected const string ToStringFormat = "Query handler for type \"{0}\" with returning type \"{1}\" could not be found.";

        /// <summary>
        /// Type of unresolved query handler.
        /// </summary>
        protected Type TypeOfQueryHandler { get; }
        /// <summary>
        /// Type of result of unresolved query handler.
        /// </summary>
        protected Type TypeOfQueryHandlerResult { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="QueryHandlerNotFoundException"/>.
        /// </summary>
        /// <param name="typeOfCommandHandler">Type of unresolved query handler.</param>
        /// <param name="typeOfQueryHandlerResult">Type of result of unresolved query handler.</param>
        public QueryHandlerNotFoundException(Type typeOfQueryHandler, Type typeOfQueryHandlerResult)
        {
            TypeOfQueryHandler = typeOfQueryHandler;
            TypeOfQueryHandlerResult = typeOfQueryHandlerResult;
        }
        /// <inheritdoc/>
        public override string ToString()
        {
            return string.Format(ToStringFormat, TypeOfQueryHandler, TypeOfQueryHandlerResult);
        }
    }
}