﻿
namespace BooksApi.Models.Contracts
{
    /// <summary>
    /// Contract for bookstore database settings.
    /// </summary>
    public interface IBookstoreDatabaseSettings
    {
        /// <summary>
        /// Collection name of books.
        /// </summary>
        string BooksCollectionName { get; set; }
        /// <summary>
        /// Connection string.
        /// </summary>
        string ConnectionString { get; set; }
        /// <summary>
        /// Name of database.
        /// </summary>
        string DatabaseName { get; set; }
    }
}