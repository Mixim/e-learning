﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Common.Interfaces.Queries;
using DAL.DbContexts;
using Processing.Queries.GetAllBooks.DTO;

namespace Processing.Queries.GetAllBooks
{
    /// <summary>
    /// Handler of getting all books.
    /// </summary>
    public class GetAllBooksHandler : IQueryHandler<GetAllBooksQuery, IEnumerable<Book>>
    {
        /// <summary>
        /// Data context.
        /// </summary>
        protected CQRSDataContext DataContext { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="GetAllBooksHandler"/>.
        /// </summary>
        /// <param name="dataContext">Data context.</param>
        public GetAllBooksHandler(CQRSDataContext dataContext)
        {
            DataContext = dataContext;
        }
        /// <inheritdoc/>
        public async Task<IEnumerable<Book>> Handle(GetAllBooksQuery query)
        {
            IEnumerable<Book> returnValue;
            returnValue = await DataContext.Books
                .Select(b => new Book(b.Id, b.Title, b.PublicationDate, b.Genre))
                .ToListAsync();
            return returnValue;
        }
    }
}